#!/usr/bin/env bash

# makeicons is a bash script to aid in the creation of application icons from an svg file.

# Copyright (C) 2023 Chris Morrison

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/.

if ! command -v inkscape &>/dev/null; then
    >&2 echo "Fatal error: this script requires the inkscape command."
fi

if ! command -v icotool &>/dev/null; then
    >&2 echo "Fatal error: this script requires the icotool command."
fi

trap 'echo -e "\rOperation was cancelled by the user."; exit 1' INT

input_file=""
exec_name=""
quiet="NO"
win_icons="NO"
mac_icons="NO"

function usage()
{
    >&2 echo "Usage: $(basename "$0") -h"
    >&2 echo "   or: $(basename "$0") -v"
    >&2 echo "   or: $(basename "$0") [ options ] -e|--exe-name <executable name> -i|--input <svg file> <icons path>"

    exit 1
}

function help()
{
    echo "Usage: $(basename "$0") -h"
    echo "   or: $(basename "$0") -v"
    echo "   or: $(basename "$0") [ options ] -e|--exe-name <executable name> -i|--input <svg file> <icons path>"
    echo "In the 1st form, display this message and exit."
    echo "In the 2nd form, display version information and exit."
    echo "In the 3rd form, creates a free desktop.org icons directory structure"
    echo "and populates it with appropriately sized png files, scaled from the."
    evho "input file."
    echo ""
    echo "Options:"
    echo "  -h, --help                  Print this message and exit."
    echo "  -v, --version               Print version information and exit."
    echo "  -i, --input <svg file>      The SVG input file."
    echo "  -e, --exe-name <name>       The name of the executable for which application"
    echo "                              icons are to be created."
    echo "  -q, --quiet                 Do not print any additional information."
    echo "  -w, --windows-ico           Output a Windows icon (.ico) file."
    
    exit 0
}

function version()
{
    echo "makeicons version 1.0.0"
    echo "Copyright (C) 2023 Chris Morrison"
    echo ""
    echo "License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
    echo ""
    echo "This is free software; you are free to change and redistribute it."
    echo "There is NO WARRANTY, to the extent permitted by law."

    exit 0
}

args=$(getopt -a -o hvi:e:qw --long help,version,input:,exe-name:quiet,windows-ico -- "$@")
if [[ $? -gt 0 ]]; then
    usage
fi

eval set -- "${args}"
while :
do
    case $1 in
        -h | --help)        help            ; shift ;;
        -v | --version)     version         ; shift ;; # shift 2 if option has an argument.
        -i | --input)       input_file=$2   ; shift 2;;
        -e | --exe-name)    exec_name=$2    ; shift 2;;
        -q | --quiet)       quiet="YES"     ; shift ;;
        -w | --windows-ico) win_icons="YES" ; shift ;;
        # -- means the end of the arguments; drop this, and break out of the while loop
        --) shift; break ;;
        *) >&2 echo Unsupported option: "$1"
        usage ;;
    esac
done

if [[ $# -eq 0 ]]; then
  usage
fi

dest_path=$(realpath "$@")

if [[ ! -d "${dest_path}" ]]; then
    echo "Fatal error: '${dest_path}' does not refer to an existing directory." >&2
    exit 1
fi
if [[ ! -f "${input_file}" ]]; then
    echo "Fatal error: '${input_file}' does not refer to an existing file." >&2
    exit 1
fi
if [[ -z "${exec_name}" ]]; then
    echo "Fatal error: executable name not specified." >&2
    exit 1
fi

sizes="8 16 20 22 24 32 36 40 42 44 48 56 64 72 80 96 128 150 192 256 310 512 1024"

if [[ "${quiet}" == "NO" ]]; then
    echo "Creating directory structure..."
fi
for size in ${sizes}; do
    mkdir -p "${dest_path}/icons/hicolor/${size}x${size}/apps" || exit 1
done

mkdir -p "${dest_path}/icons/hicolor/scalable/apps" || exit 1

if [[ "${quiet}" == "NO" ]]; then
    echo "Moving files into place..."
fi
file_list=""
for size in ${sizes}; do
    if [[ "${quiet}" == "NO" ]]; then
        inkscape -w "${size}" -h "${size}" "${input_file}" -o "${dest_path}/icons/hicolor/${size}x${size}/apps/${exec_name}.png" || exit 1
    else
        inkscape -w "${size}" -h "${size}" "${input_file}" -o "${dest_path}/icons/hicolor/${size}x${size}/apps/${exec_name}.png" 1>/dev/null || exit 1
    fi
    file_list="${file_list} ${dest_path}/icons/hicolor/${size}x${size}/apps/${exec_name}.png"
done
cp "${input_file}" "${dest_path}/icons/hicolor/scalable/apps/${exec_name}.svg" || exit 1

if [[ "${win_icons}" == "YES" ]]; then
    if [[ "${quiet}" == "NO" ]]; then
        echo "Creating Microsoft Windows ico file..."
    fi
    icotool -c -o -b 32 "${dest_path}/${exec_name}.ico" ${file_list}
fi