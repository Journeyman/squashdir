# squashdir


## Description
A collection of small shell scripts that perform useful functions and can be used in larger more complex scripts.

## Installation
The scripts can be placed anywhere in your PATH or in the same directory as your script.

## Usage
See help for each individual command for usage.

## Requirements
The 7z and uuid commands must be installed on your system.

## Authors and acknowledgment
Copyright (C) 2023 Chris Morrison

## License
See [LICENSE](https://gitlab.com/Journeyman/squashdir/-/blob/main/LICENSE).

